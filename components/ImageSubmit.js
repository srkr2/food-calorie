import React from "react";
import { Image,View,Pressable,Text } from "react-native";

const ImageSubmit = ({imageUri}) => {
  const submitPhoto = async () => {
    const formData = new FormData();
    formData.append("image", {
      uri: imageUri,
      type: "image/jpeg",
      name: "test1.jpeg",
    });
    console.log(formData);
    await axios.post(
      "https://webhook.site/05ed5390-ee6d-40aa-9a15-336412a17474",
      formData,
      {
        headers: {
          "Content-Type": "multipart/form-data",
        },
      }
    );
  };
  
  return (
    <View className="flex justify-center items-center space-y-2">
      <Image source={{ uri: imageUri }} width={200} height={200} />
      <Pressable
        className="bg-blue-500 p-2 rounded-md w-16"
        onPress={submitPhoto}
      >
        <Text className="text-white">Submit</Text>
      </Pressable>
    </View>
  );
};

export default ImageSubmit;
